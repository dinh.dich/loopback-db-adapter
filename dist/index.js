"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Created by DICH on 5/7/2018.
 */
const CassandraRepository_1 = require("./cassandra/CassandraRepository");
exports.CassandraRepository = CassandraRepository_1.default;
const ElasticRepository_1 = require("./elasticsearch/ElasticRepository");
exports.ElasticRepository = ElasticRepository_1.default;
const MongoRepository_1 = require("./mongo/MongoRepository");
exports.MongoRepository = MongoRepository_1.default;
//# sourceMappingURL=index.js.map