/**
 * Created by DICH on 5/6/2018.
 */
export interface IMongoDao {
    create(data: any, callback: any): any;
    all(filter: any, callback: any): any;
    update(where: any, data: any, callback: any): any;
    destroyAll(where: any, callback: any): any;
    findOne(where: any, callback: any): any;
    count(where: any, callback: any): any;
}
