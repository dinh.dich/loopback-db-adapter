/**
 * Created by DICH on 5/6/2018.
 */
interface IRepository {
    getCreateBuilder(): any;
    getSelectBuilder(): any;
    getUpdateBuilder(): any;
    getDeleteBuilder(): any;
    getCountBuilder(): any;
    create(info: any, cb: any): any;
    all(filter: any, cb: any): any;
    findOne(filter: any, cb: any): any;
    update(filter: any, data: any, cb: any): any;
    destroy(where: any, cb: any): any;
    count(where: any, cb: any): any;
}
export { IRepository };
