"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Created by DICH on 5/6/2018.
 */
const CassandraWhereBuilder_1 = require("./CassandraWhereBuilder");
const rx_1 = require("rx");
class CassandraSelectBuilder extends CassandraWhereBuilder_1.default {
    constructor(repos) {
        super();
        this.filter = {};
        this.cassandraRepos = repos;
    }
    order(order) {
        this.filter.order = order;
        return this;
    }
    limit(limit) {
        this.filter.limit = limit;
        return this;
    }
    offset(offset) {
        this.filter.offset = offset;
        return this;
    }
    fields(fields) {
        this.filter.fields = fields;
        return this;
    }
    options(options) {
        this.eachPageOptions = options;
        return this;
    }
    select(cb = null) {
        return new Promise((resolve, reject) => {
            this.cassandraRepos.all(Object.assign({}, this.filter, { where: this.getWhereClause() }), (error, results) => {
                cb && cb(error, results);
                if (error) {
                    reject(error);
                }
                else {
                    resolve(results);
                }
            });
        });
    }
    findOne(cb = null) {
        this.limit(1);
        return this.select()
            .then(results => {
            cb && cb(null, results[0]);
            return Promise.resolve(results[0]);
        })
            .catch(error => {
            cb && cb(error);
            return Promise.reject(error);
        });
    }
    eachPage(eachCb, endCb) {
        return rx_1.Observable.create(observer => {
            this.cassandraRepos.eachPage(Object.assign({}, this.filter, { where: this.getWhereClause() }), this.eachPageOptions, (page, rows) => {
                eachCb && eachCb(page, rows);
                observer.onNext(rows);
            }, (error, result) => {
                endCb && endCb(error, result);
                if (error) {
                    observer.onError(error);
                }
                else {
                    observer.onCompleted();
                }
            });
        });
    }
}
exports.default = CassandraSelectBuilder;
//# sourceMappingURL=CassandraSelectBuilder.js.map