"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Created by DICH on 5/6/2018.
 */
const CassandraWhereBuilder_1 = require("./CassandraWhereBuilder");
class CassandraDeleteBuilder extends CassandraWhereBuilder_1.default {
    constructor(repos) {
        super();
        this.cassandraRepos = repos;
    }
    fields(fields) {
        this.columns = fields;
        return this;
    }
    delete(cb = null) {
        return new Promise((resolve, reject) => {
            this.cassandraRepos.destroy(this.getWhereClause(), this.columns, error => {
                cb && cb(error);
                if (error) {
                    reject(error);
                }
                else {
                    resolve();
                }
            });
        });
    }
}
exports.default = CassandraDeleteBuilder;
//# sourceMappingURL=CassandraDeleteBuilder.js.map