/**
 * Created by DICH on 5/6/2018.
 */
export default class CassandraWhereBuilder {
    protected whereClause: any;
    protected getWhereClause(): {};
    where(whereClause: object): this;
    lt(key: any, value: any): this;
    lte(key: any, value: any): this;
    gt(key: any, value: any): this;
    gte(key: any, value: any): this;
    eq(key: any, value: any): this;
}
