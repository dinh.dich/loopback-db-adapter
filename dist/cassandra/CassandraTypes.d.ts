/**
 * Created by dichn on 9/16/2018.
 */
export declare type EachCb = (page: number, rows: Array<Object>) => any;
export declare type EndCb = (error: object, result: {
    nextPage: Function;
}) => any;
