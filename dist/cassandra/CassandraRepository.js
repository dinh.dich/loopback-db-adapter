"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const CassandraCreateBuilder_1 = require("./CassandraCreateBuilder");
const CassandraSelectBuilder_1 = require("./CassandraSelectBuilder");
const CassandraUpdateBuilder_1 = require("./CassandraUpdateBuilder");
const CassandraDeleteBuilder_1 = require("./CassandraDeleteBuilder");
class CassandraRepository {
    constructor(mongoDao) {
        this.cassandraDao = mongoDao;
    }
    getCreateBuilder() {
        return new CassandraCreateBuilder_1.default(this.cassandraDao);
    }
    getSelectBuilder() {
        return new CassandraSelectBuilder_1.default(this.cassandraDao);
    }
    getUpdateBuilder() {
        return new CassandraUpdateBuilder_1.default(this.cassandraDao);
    }
    getDeleteBuilder() {
        return new CassandraDeleteBuilder_1.default(this.cassandraDao);
    }
    getCountBuilder() {
        throw new Error("Method not implemented.");
    }
    getBatchBuilder() {
        return this.cassandraDao.batch();
    }
    count(where, cb = null) {
        throw new Error("Method not implemented.");
    }
    create(info, cb = null) {
        return new CassandraCreateBuilder_1.default(this.cassandraDao)
            .data(info)
            .create(cb);
    }
    all(filter, cb = null) {
        return new CassandraSelectBuilder_1.default(this.cassandraDao)
            .where(filter.where)
            .offset(filter.offset)
            .limit(filter.limit)
            .order(filter.order)
            .fields(filter.fields)
            .select(cb);
    }
    findOne(filter, cb = null) {
        return new CassandraSelectBuilder_1.default(this.cassandraDao)
            .where(filter.where)
            .limit(1)
            .order(filter.order)
            .fields(filter.fields)
            .select()
            .then(results => {
            cb && cb(null, results[0]);
            return Promise.resolve(results[0]);
        })
            .catch(error => {
            cb && cb(error);
            return Promise.reject(error);
        });
    }
    update(filter, data, cb = null) {
        return new CassandraUpdateBuilder_1.default(this.cassandraDao)
            .where(filter.where)
            .data(data)
            .update(cb);
    }
    destroy(where, cb = null) {
        return new CassandraDeleteBuilder_1.default(this.cassandraDao)
            .where(where)
            .delete(cb);
    }
    eachPage(filter, options, eachCb, endCb) {
        return new CassandraSelectBuilder_1.default(this.cassandraDao)
            .where(filter.where)
            .offset(filter.offset)
            .limit(filter.limit)
            .order(filter.order)
            .fields(filter.fields)
            .options(options)
            .eachPage(eachCb, endCb);
    }
}
exports.default = CassandraRepository;
//# sourceMappingURL=CassandraRepository.js.map