/**
 * Created by DICH on 5/6/2018.
 */
import CassandraWhereBuilder from "./CassandraWhereBuilder";
import { ICassandraDao } from "../ICassandraDao";
export default class CassandraDeleteBuilder extends CassandraWhereBuilder {
    private cassandraRepos;
    private columns;
    constructor(repos: ICassandraDao);
    fields(fields: any): CassandraDeleteBuilder;
    delete(cb?: any): Promise<any>;
}
