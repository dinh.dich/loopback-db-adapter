/**
 * Created by DICH on 5/6/2018.
 */
import { ICassandraDao } from "../ICassandraDao";
export default class CassandraCreateBuilder {
    private cassandraRepos;
    private info;
    constructor(repos: ICassandraDao);
    data(info: any): CassandraCreateBuilder;
    create(cb?: any): Promise<any>;
}
