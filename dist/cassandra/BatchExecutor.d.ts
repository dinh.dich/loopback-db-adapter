/**
 * Created by DichVD on 5/7/2018.
 */
declare abstract class BatchExecutor {
    abstract create(data: any, options: any): BatchExecutor;
    abstract update(where: any, data: any, checkExists: any): BatchExecutor;
    abstract delete(where: any, columns: any): BatchExecutor;
    abstract execute(cb: any): Promise<any>;
}
export default BatchExecutor;
