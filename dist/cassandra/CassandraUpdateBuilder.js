"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Created by DICH on 5/6/2018.
 */
const CassandraWhereBuilder_1 = require("./CassandraWhereBuilder");
class CassandraUpdateBuilder extends CassandraWhereBuilder_1.default {
    constructor(repos) {
        super();
        this.updateInfo = {};
        this.isUpsert = true;
        this.cassandraRepos = repos;
    }
    data(info) {
        this.updateInfo = info;
        return this;
    }
    upsert(isUpsert) {
        this.isUpsert = isUpsert;
        return this;
    }
    update(cb = null) {
        return new Promise((resolve, reject) => {
            this.cassandraRepos.update(this.getWhereClause(), this.updateInfo, !this.isUpsert, error => {
                cb && cb(error);
                if (error) {
                    reject(error);
                }
                else {
                    resolve();
                }
            });
        });
    }
}
exports.default = CassandraUpdateBuilder;
//# sourceMappingURL=CassandraUpdateBuilder.js.map