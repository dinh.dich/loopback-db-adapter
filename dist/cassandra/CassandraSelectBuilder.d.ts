/**
 * Created by DICH on 5/6/2018.
 */
import CassandraWhereBuilder from "./CassandraWhereBuilder";
import { ICassandraDao } from "../ICassandraDao";
import { Observable } from 'rx';
import { EachCb, EndCb } from './CassandraTypes';
export default class CassandraSelectBuilder extends CassandraWhereBuilder {
    private cassandraRepos;
    private filter;
    private eachPageOptions;
    constructor(repos: ICassandraDao);
    order(order: any): CassandraSelectBuilder;
    limit(limit: any): CassandraSelectBuilder;
    offset(offset: any): CassandraSelectBuilder;
    fields(fields: any): CassandraSelectBuilder;
    options(options: {
        autoPage: boolean;
        fetchSize: number;
    }): CassandraSelectBuilder;
    select(cb?: any): Promise<Array<any>>;
    findOne(cb?: any): Promise<any>;
    eachPage(eachCb: EachCb, endCb: EndCb): Observable;
}
