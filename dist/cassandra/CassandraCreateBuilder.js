"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class CassandraCreateBuilder {
    constructor(repos) {
        this.info = {};
        this.cassandraRepos = repos;
    }
    data(info) {
        this.info = info;
        return this;
    }
    create(cb = null) {
        return new Promise((resolve, reject) => {
            this.cassandraRepos.create(this.info, error => {
                cb && cb(error);
                if (error) {
                    reject(error);
                }
                else {
                    resolve();
                }
            });
        });
    }
}
exports.default = CassandraCreateBuilder;
//# sourceMappingURL=CassandraCreateBuilder.js.map