"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Created by DICH on 5/6/2018.
 */
class CassandraWhereBuilder {
    constructor() {
        this.whereClause = {};
    }
    getWhereClause() {
        if (this.whereClause && Object.keys(this.whereClause).length > 0)
            return this.whereClause;
    }
    where(whereClause) {
        this.whereClause = whereClause;
        return this;
    }
    lt(key, value) {
        this.whereClause = Object.assign({}, this.whereClause, { [key]: { 'lt': value } });
        return this;
    }
    lte(key, value) {
        this.whereClause = Object.assign({}, this.whereClause, { [key]: { 'lte': value } });
        return this;
    }
    gt(key, value) {
        this.whereClause = Object.assign({}, this.whereClause, { [key]: { 'gt': value } });
        return this;
    }
    gte(key, value) {
        this.whereClause = Object.assign({}, this.whereClause, { [key]: { 'gte': value } });
        return this;
    }
    eq(key, value) {
        this.whereClause = Object.assign({}, this.whereClause, { [key]: { 'eq': value } });
        return this;
    }
}
exports.default = CassandraWhereBuilder;
//# sourceMappingURL=CassandraWhereBuilder.js.map