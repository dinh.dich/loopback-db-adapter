/**
 * Created by DICH on 5/6/2018.
 */
import CassandraWhereBuilder from "./CassandraWhereBuilder";
import { ICassandraDao } from "../ICassandraDao";
export default class CassandraUpdateBuilder extends CassandraWhereBuilder {
    private cassandraRepos;
    private updateInfo;
    private isUpsert;
    constructor(repos: ICassandraDao);
    data(info: any): CassandraUpdateBuilder;
    upsert(isUpsert: any): CassandraUpdateBuilder;
    update(cb?: any): Promise<any>;
}
