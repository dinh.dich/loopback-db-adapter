/**
 * Created by DICH on 5/6/2018.
 */
import { IRepository } from "../IRepository";
import { ICassandraDao } from "../ICassandraDao";
import CassandraCreateBuilder from "./CassandraCreateBuilder";
import CassandraSelectBuilder from "./CassandraSelectBuilder";
import CassandraUpdateBuilder from "./CassandraUpdateBuilder";
import CassandraDeleteBuilder from "./CassandraDeleteBuilder";
import BatchExecutor from "./BatchExecutor";
import { Observable } from 'rx';
export default class CassandraRepository implements IRepository {
    private cassandraDao;
    constructor(mongoDao: ICassandraDao);
    getCreateBuilder(): CassandraCreateBuilder;
    getSelectBuilder(): CassandraSelectBuilder;
    getUpdateBuilder(): CassandraUpdateBuilder;
    getDeleteBuilder(): CassandraDeleteBuilder;
    getCountBuilder(): any;
    getBatchBuilder(): BatchExecutor;
    count(where: any, cb?: any): any;
    create(info: any, cb?: any): Promise<any>;
    all(filter: any, cb?: any): Promise<Array<any>>;
    findOne(filter: any, cb?: any): Promise<object>;
    update(filter: any, data: any, cb?: any): Promise<any>;
    destroy(where: any, cb?: any): Promise<any>;
    eachPage(filter: any, options: any, eachCb: any, endCb: any): Observable;
}
