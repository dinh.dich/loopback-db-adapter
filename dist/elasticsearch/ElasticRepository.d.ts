/**
 * Created by DICH on 5/6/2018.
 */
import { IRepository } from "../IRepository";
import { IElasticDao } from "../IElasticDao";
import ElasticCreateBuilder from "./ElasticCreateBuilder";
import ElasticSelectBuilder from "./ElasticSelectBuilder";
import ElasticUpdateBuilder from "./ElasticUpdateBuilder";
import ElasticDeleteBuilder from "./ElasticDeleteBuilder";
import ElasticCountBuilder from "./ElasticCountBuilder";
export default class ElasticRepository implements IRepository {
    private elasticDao;
    constructor(mongoDao: IElasticDao);
    getCreateBuilder(): ElasticCreateBuilder;
    getSelectBuilder(): ElasticSelectBuilder;
    getUpdateBuilder(): ElasticUpdateBuilder;
    getDeleteBuilder(): ElasticDeleteBuilder;
    getCountBuilder(): ElasticCountBuilder;
    create(info: any, cb?: any): Promise<any>;
    all(filter: any, cb?: any): Promise<any>;
    findOne(filter: any, cb?: any): Promise<any>;
    update(filter: any, data: any, cb?: any): Promise<any>;
    destroy(where: any, cb?: any): Promise<any>;
    count(where: any, cb?: any): Promise<any>;
}
