"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Created by DICH on 5/6/2018.
 */
const ElasticWhereBuilder_1 = require("./ElasticWhereBuilder");
class ElasticDeleteBuilder extends ElasticWhereBuilder_1.default {
    constructor(repos) {
        super();
        this.elasticRepos = repos;
    }
    delete(cb = null) {
        return new Promise((resolve, reject) => {
            this.elasticRepos.destroyAll(this.whereClause, error => {
                cb && cb(error);
                if (error) {
                    reject(error);
                }
                else {
                    resolve();
                }
            });
        });
    }
}
exports.default = ElasticDeleteBuilder;
//# sourceMappingURL=ElasticDeleteBuilder.js.map