/**
 * Created by DICH on 5/6/2018.
 */
import ElasticWhereBuilder from "./ElasticWhereBuilder";
import { IElasticDao } from "../IElasticDao";
export default class ElasticCountBuilder extends ElasticWhereBuilder {
    private elasticRepos;
    constructor(repos: IElasticDao);
    count(cb?: any): Promise<any>;
}
