/**
 * Created by DICH on 5/6/2018.
 */
export default class ElasticWhereBuilder {
    protected whereClause: any;
    protected matchClause: any;
    private mode;
    where(whereClause: any): this;
    match(matchClause: any): this;
    lt(key: any, value: any): this;
    lte(key: any, value: any): this;
    gt(key: any, value: any): this;
    gte(key: any, value: any): this;
    eq(key: any, value: any): this;
    neq(key: any, value: any): this;
    between(key: any, values: any): this;
    in(key: any, values: any): this;
    nin(key: any, values: any): this;
    exists(key: any, value: any): this;
    or(key: any, value: any): this;
    and(key: any, value: any): this;
    script(script: any): this;
}
