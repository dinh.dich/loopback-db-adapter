"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Created by DICH on 5/6/2018.
 */
const ElasticWhereBuilder_1 = require("./ElasticWhereBuilder");
class ElasticUpdateBuilder extends ElasticWhereBuilder_1.default {
    constructor(repos) {
        super();
        this.updateInfo = {};
        this.isUpsert = false;
        this.useScript = false;
        this.elasticRepos = repos;
    }
    data(info) {
        this.updateInfo = info;
        return this;
    }
    upsert(isUpsert) {
        this.isUpsert = isUpsert;
        return this;
    }
    increase(key, value) {
        this.updateInfo = Object.assign({}, this.updateInfo, { [key]: { '$add': value } });
        this.useScript = true;
        return this;
    }
    decrease(key, value) {
        this.updateInfo = Object.assign({}, this.updateInfo, { [key]: { '$sub': value } });
        this.useScript = true;
        return this;
    }
    id(id) {
        this.docId = id;
        return this;
    }
    update(cb = null) {
        if (this.whereClause && this.whereClause['$script']) {
            return new Promise((resolve, reject) => {
                this.elasticRepos.updateAllScript(this.whereClause, this.updateInfo, null, error => {
                    cb && cb(error);
                    if (error) {
                        return reject(error);
                    }
                    resolve();
                });
            });
        }
        else if (this.useScript && !this.isUpsert) {
            return new Promise((resolve, reject) => {
                this.elasticRepos.updateScript(this.docId, this.updateInfo, error => {
                    cb && cb(error);
                    if (error) {
                        return reject(error);
                    }
                    resolve();
                });
            });
        }
        else if (this.useScript && this.isUpsert) {
            return new Promise((resolve, reject) => {
                this.elasticRepos.updateOrCreateScript(this.docId, this.updateInfo, error => {
                    cb && cb(error);
                    if (error) {
                        return reject(error);
                    }
                    resolve();
                });
            });
        }
        else {
            return new Promise((resolve, reject) => {
                this.elasticRepos.updateAll(this.whereClause, this.updateInfo, null, (error, count) => {
                    if (error) {
                        cb && cb(error);
                        reject(error);
                    }
                    else {
                        if (!count && this.isUpsert) {
                            this.elasticRepos.create(this.updateInfo, error => {
                                cb && cb(error);
                                if (error) {
                                    reject(error);
                                }
                                else {
                                    resolve({ count });
                                }
                            });
                        }
                        else {
                            cb && cb(null, { count });
                            resolve({ count });
                        }
                    }
                });
            });
        }
    }
}
exports.default = ElasticUpdateBuilder;
//# sourceMappingURL=ElasticUpdateBuilder.js.map