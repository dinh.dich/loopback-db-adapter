"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const ElasticCreateBuilder_1 = require("./ElasticCreateBuilder");
const ElasticSelectBuilder_1 = require("./ElasticSelectBuilder");
const ElasticUpdateBuilder_1 = require("./ElasticUpdateBuilder");
const ElasticDeleteBuilder_1 = require("./ElasticDeleteBuilder");
const ElasticCountBuilder_1 = require("./ElasticCountBuilder");
class ElasticRepository {
    constructor(mongoDao) {
        this.elasticDao = mongoDao;
    }
    getCreateBuilder() {
        return new ElasticCreateBuilder_1.default(this.elasticDao);
    }
    getSelectBuilder() {
        return new ElasticSelectBuilder_1.default(this.elasticDao);
    }
    getUpdateBuilder() {
        return new ElasticUpdateBuilder_1.default(this.elasticDao);
    }
    getDeleteBuilder() {
        return new ElasticDeleteBuilder_1.default(this.elasticDao);
    }
    getCountBuilder() {
        return new ElasticCountBuilder_1.default(this.elasticDao);
    }
    create(info, cb = null) {
        return new ElasticCreateBuilder_1.default(this.elasticDao)
            .data(info)
            .create(cb);
    }
    all(filter, cb = null) {
        return new ElasticSelectBuilder_1.default(this.elasticDao)
            .where(filter.where)
            .offset(filter.offset)
            .limit(filter.limit)
            .order(filter.order)
            .fields(filter.fields)
            .select(cb);
    }
    findOne(filter, cb = null) {
        return new ElasticSelectBuilder_1.default(this.elasticDao)
            .where(filter.where)
            .limit(1)
            .order(filter.order)
            .fields(filter.fields)
            .select()
            .then(results => {
            cb && cb(null, results[0]);
            return Promise.resolve(results[0]);
        })
            .catch(error => {
            cb && cb(error);
            return Promise.reject(error);
        });
    }
    update(filter, data, cb = null) {
        return new ElasticUpdateBuilder_1.default(this.elasticDao)
            .where(filter.where)
            .data(data)
            .update(cb);
    }
    destroy(where, cb = null) {
        return new ElasticDeleteBuilder_1.default(this.elasticDao)
            .where(where)
            .delete(cb);
    }
    count(where, cb = null) {
        return new ElasticCountBuilder_1.default(this.elasticDao)
            .where(where)
            .count(cb);
    }
}
exports.default = ElasticRepository;
//# sourceMappingURL=ElasticRepository.js.map