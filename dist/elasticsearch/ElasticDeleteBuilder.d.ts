/**
 * Created by DICH on 5/6/2018.
 */
import ElasticWhereBuilder from "./ElasticWhereBuilder";
import { IElasticDao } from "../IElasticDao";
export default class ElasticDeleteBuilder extends ElasticWhereBuilder {
    private elasticRepos;
    constructor(repos: IElasticDao);
    delete(cb?: any): Promise<any>;
}
