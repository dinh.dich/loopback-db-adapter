/**
 * Created by DICH on 5/6/2018.
 */
import ElasticWhereBuilder from "./ElasticWhereBuilder";
import { IElasticDao } from "../IElasticDao";
export default class ElasticSelectBuilder extends ElasticWhereBuilder {
    private elasticRepos;
    private filter;
    constructor(repos: IElasticDao);
    order(order: any): ElasticSelectBuilder;
    limit(limit: any): ElasticSelectBuilder;
    offset(offset: any): ElasticSelectBuilder;
    fields(fields: any): ElasticSelectBuilder;
    /**
     * add function score
     * @param score
     * @returns {ElasticSelectBuilder}
     */
    score(score: any): ElasticSelectBuilder;
    select(cb?: any): Promise<any>;
    findOne(cb?: any): Promise<any>;
}
