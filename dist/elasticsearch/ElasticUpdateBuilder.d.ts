/**
 * Created by DICH on 5/6/2018.
 */
import ElasticWhereBuilder from "./ElasticWhereBuilder";
import { IElasticDao } from "../IElasticDao";
export default class ElasticUpdateBuilder extends ElasticWhereBuilder {
    private elasticRepos;
    private updateInfo;
    private isUpsert;
    private useScript;
    private docId;
    constructor(repos: IElasticDao);
    data(info: any): ElasticUpdateBuilder;
    upsert(isUpsert: any): ElasticUpdateBuilder;
    increase(key: any, value: any): ElasticUpdateBuilder;
    decrease(key: any, value: any): ElasticUpdateBuilder;
    id(id: any): ElasticUpdateBuilder;
    update(cb?: any): Promise<any>;
}
