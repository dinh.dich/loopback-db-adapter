/**
 * Created by DICH on 5/6/2018.
 */
import { IElasticDao } from "../IElasticDao";
export default class ElasticCreateBuilder {
    private elasticRepos;
    private info;
    constructor(repos: IElasticDao);
    data(info: any): ElasticCreateBuilder;
    create(cb?: any): Promise<any>;
}
