"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Created by DICH on 5/6/2018.
 */
const ElasticWhereBuilder_1 = require("./ElasticWhereBuilder");
class ElasticSelectBuilder extends ElasticWhereBuilder_1.default {
    constructor(repos) {
        super();
        this.filter = {};
        this.elasticRepos = repos;
    }
    order(order) {
        this.filter.order = order;
        return this;
    }
    limit(limit) {
        this.filter.limit = limit;
        return this;
    }
    offset(offset) {
        this.filter.offset = offset;
        return this;
    }
    fields(fields) {
        this.filter.fields = fields;
        return this;
    }
    /**
     * add function score
     * @param score
     * @returns {ElasticSelectBuilder}
     */
    score(score) {
        this.filter.score = score;
        return this;
    }
    select(cb = null) {
        return new Promise((resolve, reject) => {
            this.elasticRepos.all(Object.assign({}, this.filter, { where: this.whereClause, match: this.matchClause }), (error, results) => {
                cb && cb(error, results);
                if (error) {
                    reject(error);
                }
                else {
                    resolve(results);
                }
            });
        });
    }
    findOne(cb = null) {
        this.limit(1);
        return this.select()
            .then(results => {
            cb && cb(null, results[0]);
            return Promise.resolve(results[0]);
        })
            .catch(error => {
            cb && cb(error);
            return Promise.reject(error);
        });
    }
}
exports.default = ElasticSelectBuilder;
//# sourceMappingURL=ElasticSelectBuilder.js.map