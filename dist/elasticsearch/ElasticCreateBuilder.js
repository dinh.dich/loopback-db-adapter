"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class ElasticCreateBuilder {
    constructor(repos) {
        this.info = {};
        this.elasticRepos = repos;
    }
    data(info) {
        this.info = info;
        return this;
    }
    create(cb = null) {
        return new Promise((resolve, reject) => {
            this.elasticRepos.create(this.info, error => {
                cb && cb(error);
                if (error) {
                    reject(error);
                }
                else {
                    resolve();
                }
            });
        });
    }
}
exports.default = ElasticCreateBuilder;
//# sourceMappingURL=ElasticCreateBuilder.js.map