"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Created by DICH on 5/6/2018.
 */
class ElasticWhereBuilder {
    constructor() {
        this.whereClause = {};
        this.matchClause = {};
        this.mode = 0;
    }
    where(whereClause) {
        this.whereClause = whereClause || this.whereClause;
        this.mode = 0;
        return this;
    }
    match(matchClause) {
        this.matchClause = matchClause || this.matchClause;
        this.mode = 1;
        return this;
    }
    lt(key, value) {
        if (this.mode == 0) {
            this.whereClause = Object.assign({}, this.whereClause, { [key]: { 'lt': value } });
        }
        else {
            this.matchClause = Object.assign({}, this.matchClause, { [key]: { 'lt': value } });
        }
        return this;
    }
    lte(key, value) {
        if (this.mode == 0) {
            this.whereClause = Object.assign({}, this.whereClause, { [key]: { 'lte': value } });
        }
        else {
            this.matchClause = Object.assign({}, this.matchClause, { [key]: { 'lte': value } });
        }
        return this;
    }
    gt(key, value) {
        if (this.mode == 0) {
            this.whereClause = Object.assign({}, this.whereClause, { [key]: { 'gt': value } });
        }
        else {
            this.matchClause = Object.assign({}, this.matchClause, { [key]: { 'gt': value } });
        }
        return this;
    }
    gte(key, value) {
        if (this.mode == 0) {
            this.whereClause = Object.assign({}, this.whereClause, { [key]: { 'gte': value } });
        }
        else {
            this.matchClause = Object.assign({}, this.matchClause, { [key]: { 'gte': value } });
        }
        return this;
    }
    eq(key, value) {
        if (this.mode == 0) {
            this.whereClause = Object.assign({}, this.whereClause, { [key]: value });
        }
        else {
            this.matchClause = Object.assign({}, this.matchClause, { [key]: value });
        }
        return this;
    }
    neq(key, value) {
        if (this.mode == 0) {
            this.whereClause = Object.assign({}, this.whereClause, { [key]: { 'neq': value } });
        }
        else {
            this.matchClause = Object.assign({}, this.matchClause, { [key]: { 'neq': value } });
        }
        return this;
    }
    between(key, values) {
        if (this.mode == 0) {
            this.whereClause = Object.assign({}, this.whereClause, { [key]: { 'between': values } });
        }
        else {
            this.matchClause = Object.assign({}, this.matchClause, { [key]: { 'between': values } });
        }
        return this;
    }
    in(key, values) {
        if (this.mode == 0) {
            this.whereClause = Object.assign({}, this.whereClause, { [key]: { 'inq': values } });
        }
        else {
            this.matchClause = Object.assign({}, this.matchClause, { [key]: { 'inq': values } });
        }
        return this;
    }
    nin(key, values) {
        if (this.mode == 0) {
            this.whereClause = Object.assign({}, this.whereClause, { [key]: { 'nin': values } });
        }
        else {
            this.matchClause = Object.assign({}, this.matchClause, { [key]: { 'nin': values } });
        }
        return this;
    }
    exists(key, value) {
        if (this.mode == 0) {
            this.whereClause = Object.assign({}, this.whereClause, { [key]: { exists: value } });
        }
        else {
            this.matchClause = Object.assign({}, this.matchClause, { [key]: { exists: value } });
        }
        return this;
    }
    or(key, value) {
        if (this.mode == 0) {
            this.whereClause = Object.assign({}, this.whereClause, { or: [...(this.whereClause.or || []), { [key]: value }] });
        }
        else {
            this.matchClause = Object.assign({}, this.matchClause, { or: [...(this.matchClause.or || []), { [key]: value }] });
        }
        return this;
    }
    and(key, value) {
        if (this.mode == 0) {
            this.whereClause = Object.assign({}, this.whereClause, { and: [...(this.whereClause.and || []), { [key]: value }] });
        }
        else {
            this.matchClause = Object.assign({}, this.matchClause, { and: [...(this.matchClause.and || []), { [key]: value }] });
        }
        return this;
    }
    script(script) {
        if (this.mode == 0) {
            this.whereClause = Object.assign({}, this.whereClause, { '$script': script });
        }
        else {
            this.matchClause = Object.assign({}, this.matchClause, { '$script': script });
        }
        return this;
    }
}
exports.default = ElasticWhereBuilder;
//# sourceMappingURL=ElasticWhereBuilder.js.map