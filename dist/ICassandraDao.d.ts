/**
 * Created by DICH on 5/6/2018.
 */
interface ICassandraDao {
    create(data: any, callback: any): any;
    all(filter: any, callback: any): any;
    update(where: any, data: any, checkExists: any, callback: any): any;
    destroy(where: any, columns: any, callback: any): any;
    eachPage(filter: any, options: any, eachCb: any, endCb: any): any;
    batch(): any;
}
export { ICassandraDao };
