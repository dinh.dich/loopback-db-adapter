/**
 * Created by DichVD on 5/7/2018.
 */
export interface IElasticDao {
    create(data: any, callback: any): any;
    all(filter: any, callback: any): any;
    updateAll(where: any, data: any, options: any, callback: any): any;
    updateOrCreate(data: any, callback: any): any;
    updateAllScript(where: any, data: any, options: any, callback: any): any;
    updateScript(id: any, data: any, callback: any): any;
    updateOrCreateScript(id: any, data: any, callback: any): any;
    destroyAll(where: any, callback: any): any;
    find(id: any, callback: any): any;
    destroy(id: any, callback: any): any;
    exists(id: any, callback: any): any;
    save(data: any, callback: any): any;
    search(query: any, callback: any): any;
    update(query: any, callback: any): any;
    count(where: any, callback: any): any;
}
