"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Created by DICH on 5/6/2018.
 */
class MongoWhereBuilder {
    constructor() {
        this.whereClause = {};
    }
    where(whereClause) {
        this.whereClause = whereClause;
        return this;
    }
    lt(key, value) {
        this.whereClause = Object.assign({}, this.whereClause, { [key]: { 'lt': value } });
        return this;
    }
    lte(key, value) {
        this.whereClause = Object.assign({}, this.whereClause, { [key]: { 'lte': value } });
        return this;
    }
    gt(key, value) {
        this.whereClause = Object.assign({}, this.whereClause, { [key]: { 'gt': value } });
        return this;
    }
    gte(key, value) {
        this.whereClause = Object.assign({}, this.whereClause, { [key]: { 'gte': value } });
        return this;
    }
    eq(key, value) {
        this.whereClause = Object.assign({}, this.whereClause, { [key]: { 'eq': value } });
        return this;
    }
    neq(key, value) {
        this.whereClause = Object.assign({}, this.whereClause, { [key]: { 'neq': value } });
        return this;
    }
    between(key, values) {
        this.whereClause = Object.assign({}, this.whereClause, { [key]: { 'between': values } });
        return this;
    }
    in(key, values) {
        this.whereClause = Object.assign({}, this.whereClause, { [key]: { 'in': values } });
        return this;
    }
    nin(key, values) {
        this.whereClause = Object.assign({}, this.whereClause, { [key]: { 'nin': values } });
        return this;
    }
    exists(key, value) {
        this.whereClause = Object.assign({}, this.whereClause, { [key]: { exists: value } });
        return this;
    }
}
exports.default = MongoWhereBuilder;
//# sourceMappingURL=MongoWhereBuilder.js.map