/**
 * Created by DICH on 5/6/2018.
 */
import MongoWhereBuilder from "./MongoWhereBuilder";
import { IMongoDao } from "../IMongoDao";
export default class MongoCountBuilder extends MongoWhereBuilder {
    private mongoRepos;
    constructor(repos: IMongoDao);
    count(cb?: any): Promise<number>;
}
