/**
 * Created by DICH on 5/6/2018.
 */
import { IMongoDao } from "../IMongoDao";
export default class MongoCreateBuilder {
    private mongoRepos;
    private info;
    constructor(repos: IMongoDao);
    data(info: object): MongoCreateBuilder;
    create(cb?: any): Promise<object>;
}
