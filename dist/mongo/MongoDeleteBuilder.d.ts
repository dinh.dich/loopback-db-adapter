/**
 * Created by DICH on 5/6/2018.
 */
import { IMongoDao } from "../IMongoDao";
import MongoWhereBuilder from "./MongoWhereBuilder";
export default class MongoDeleteBuilder extends MongoWhereBuilder {
    private mongoRepos;
    constructor(repos: IMongoDao);
    delete(cb?: any): Promise<any>;
}
