"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const MongoWhereBuilder_1 = require("./MongoWhereBuilder");
class MongoUpdateBuilder extends MongoWhereBuilder_1.default {
    constructor(repos) {
        super();
        this.updateInfo = {};
        this.isUpsert = false;
        this.repos = repos;
    }
    data(info) {
        this.updateInfo = info;
        return this;
    }
    set(info) {
        this.updateInfo['$set'] = info;
        return this;
    }
    unset(info) {
        this.updateInfo['$unset'] = info;
        return this;
    }
    pull(info) {
        this.updateInfo['$pull'] = info;
        return this;
    }
    push(info) {
        this.updateInfo['$push'] = info;
        return this;
    }
    pushAll(info) {
        this.updateInfo['$pushAll'] = info;
        return this;
    }
    upsert(isUpsert) {
        this.isUpsert = isUpsert;
        return this;
    }
    update(cb = null) {
        return new Promise((resolve, reject) => {
            this.repos.update(this.whereClause, this.updateInfo, (error, { count }) => {
                if (error) {
                    cb && cb(error);
                    reject(error);
                }
                else {
                    if (!count && this.isUpsert) {
                        this.repos.create(this.updateInfo, error => {
                            cb && cb(error);
                            if (error) {
                                reject(error);
                            }
                            else {
                                resolve({ count });
                            }
                        });
                    }
                    else {
                        cb && cb(null, { count });
                        resolve({ count });
                    }
                }
            });
        });
    }
}
exports.default = MongoUpdateBuilder;
//# sourceMappingURL=MongoUpdateBuilder.js.map