/**
 * Created by DICH on 5/6/2018.
 */
import { IRepository } from "../IRepository";
import { IMongoDao } from "../IMongoDao";
import MongoCreateBuilder from "./MongoCreateBuilder";
import MongoSelectBuilder from "./MongoSelectBuilder";
import MongoUpdateBuilder from "./MongoUpdateBuilder";
import MongoDeleteBuilder from "./MongoDeleteBuilder";
import MongoCountBuilder from "./MongoCountBuilder";
export default class MongoRepository implements IRepository {
    private mongoDao;
    constructor(mongoDao: IMongoDao);
    getCreateBuilder(): MongoCreateBuilder;
    getSelectBuilder(): MongoSelectBuilder;
    getUpdateBuilder(): MongoUpdateBuilder;
    getDeleteBuilder(): MongoDeleteBuilder;
    getCountBuilder(): MongoCountBuilder;
    create(info: any, cb?: any): Promise<object>;
    all(filter: any, cb?: any): Promise<any>;
    findOne(filter: any, cb?: any): Promise<any>;
    update(filter: any, data: any, cb?: any): Promise<any>;
    destroy(where: any, cb?: any): Promise<any>;
    count(where: any, cb?: any): Promise<any>;
}
