"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const MongoWhereBuilder_1 = require("./MongoWhereBuilder");
class MongoSelectBuilder extends MongoWhereBuilder_1.default {
    constructor(repos) {
        super();
        this.filter = {};
        this.mongoRepos = repos;
    }
    order(order) {
        this.filter.order = order;
        return this;
    }
    limit(limit) {
        this.filter.limit = limit;
        return this;
    }
    offset(offset) {
        this.filter.offset = offset;
        return this;
    }
    fields(fields) {
        this.filter.fields = fields;
        return this;
    }
    select(cb = null) {
        return new Promise((resolve, reject) => {
            this.mongoRepos.all(Object.assign({}, this.filter, { where: this.whereClause }), (error, results) => {
                cb && cb(error, results);
                if (error) {
                    reject(error);
                }
                else {
                    resolve(results);
                }
            });
        });
    }
    findOne(cb = null) {
        this.limit(1);
        return this.select()
            .then(results => {
            cb && cb(null, results[0]);
            return Promise.resolve(results[0]);
        })
            .catch(error => {
            cb && cb(error);
            return Promise.reject(error);
        });
    }
}
exports.default = MongoSelectBuilder;
//# sourceMappingURL=MongoSelectBuilder.js.map