"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class MongoCreateBuilder {
    constructor(repos) {
        this.info = {};
        this.mongoRepos = repos;
    }
    data(info) {
        this.info = info;
        return this;
    }
    create(cb = null) {
        return new Promise((resolve, reject) => {
            this.mongoRepos.create(this.info, (error, info) => {
                cb && cb(error, info);
                if (error) {
                    reject(error);
                }
                else {
                    resolve(info);
                }
            });
        });
    }
}
exports.default = MongoCreateBuilder;
//# sourceMappingURL=MongoCreateBuilder.js.map