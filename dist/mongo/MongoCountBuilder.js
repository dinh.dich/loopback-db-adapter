"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Created by DICH on 5/6/2018.
 */
const MongoWhereBuilder_1 = require("./MongoWhereBuilder");
class MongoCountBuilder extends MongoWhereBuilder_1.default {
    constructor(repos) {
        super();
        this.mongoRepos = repos;
    }
    count(cb = null) {
        return new Promise((resolve, reject) => {
            this.mongoRepos.count(this.whereClause, (error, count) => {
                cb && cb(error, count);
                if (error) {
                    reject(error);
                }
                else {
                    resolve(count);
                }
            });
        });
    }
}
exports.default = MongoCountBuilder;
//# sourceMappingURL=MongoCountBuilder.js.map