"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const MongoWhereBuilder_1 = require("./MongoWhereBuilder");
class MongoDeleteBuilder extends MongoWhereBuilder_1.default {
    constructor(repos) {
        super();
        this.mongoRepos = repos;
    }
    delete(cb = null) {
        return new Promise((resolve, reject) => {
            this.mongoRepos.destroyAll(this.whereClause, error => {
                cb && cb(error);
                if (error) {
                    reject(error);
                }
                else {
                    resolve();
                }
            });
        });
    }
}
exports.default = MongoDeleteBuilder;
//# sourceMappingURL=MongoDeleteBuilder.js.map