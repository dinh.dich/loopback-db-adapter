/**
 * Created by DICH on 5/6/2018.
 */
import { IMongoDao } from "../IMongoDao";
import MongoWhereBuilder from "./MongoWhereBuilder";
export default class MongoSelectBuilder extends MongoWhereBuilder {
    private mongoRepos;
    private filter;
    constructor(repos: IMongoDao);
    order(order: any): MongoSelectBuilder;
    limit(limit: any): MongoSelectBuilder;
    offset(offset: any): MongoSelectBuilder;
    fields(fields: any): MongoSelectBuilder;
    select(cb?: any): Promise<any>;
    findOne(cb?: any): Promise<any>;
}
