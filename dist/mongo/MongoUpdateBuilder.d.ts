/**
 * Created by DICH on 5/6/2018.
 */
import { IMongoDao } from "../IMongoDao";
import MongoWhereBuilder from "./MongoWhereBuilder";
export default class MongoUpdateBuilder extends MongoWhereBuilder {
    private repos;
    private updateInfo;
    private isUpsert;
    constructor(repos: IMongoDao);
    data(info: any): MongoUpdateBuilder;
    set(info: any): MongoUpdateBuilder;
    unset(info: any): MongoUpdateBuilder;
    pull(info: any): MongoUpdateBuilder;
    push(info: any): MongoUpdateBuilder;
    pushAll(info: any): MongoUpdateBuilder;
    upsert(isUpsert: any): MongoUpdateBuilder;
    update(cb?: any): Promise<any>;
}
