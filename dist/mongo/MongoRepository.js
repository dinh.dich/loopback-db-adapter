"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const MongoCreateBuilder_1 = require("./MongoCreateBuilder");
const MongoSelectBuilder_1 = require("./MongoSelectBuilder");
const MongoUpdateBuilder_1 = require("./MongoUpdateBuilder");
const MongoDeleteBuilder_1 = require("./MongoDeleteBuilder");
const MongoCountBuilder_1 = require("./MongoCountBuilder");
class MongoRepository {
    constructor(mongoDao) {
        this.mongoDao = mongoDao;
    }
    getCreateBuilder() {
        return new MongoCreateBuilder_1.default(this.mongoDao);
    }
    getSelectBuilder() {
        return new MongoSelectBuilder_1.default(this.mongoDao);
    }
    getUpdateBuilder() {
        return new MongoUpdateBuilder_1.default(this.mongoDao);
    }
    getDeleteBuilder() {
        return new MongoDeleteBuilder_1.default(this.mongoDao);
    }
    getCountBuilder() {
        return new MongoCountBuilder_1.default(this.mongoDao);
    }
    create(info, cb = null) {
        return new MongoCreateBuilder_1.default(this.mongoDao)
            .data(info)
            .create(cb);
    }
    all(filter, cb = null) {
        return new MongoSelectBuilder_1.default(this.mongoDao)
            .where(filter.where)
            .offset(filter.offset)
            .limit(filter.limit)
            .order(filter.order)
            .fields(filter.fields)
            .select(cb);
    }
    findOne(filter, cb = null) {
        return new MongoSelectBuilder_1.default(this.mongoDao)
            .where(filter.where)
            .limit(1)
            .order(filter.order)
            .fields(filter.fields)
            .select()
            .then(results => {
            cb && cb(null, results[0]);
            return Promise.resolve(results[0]);
        })
            .catch(error => {
            cb && cb(error);
            return Promise.reject(error);
        });
    }
    update(filter, data, cb = null) {
        return new MongoUpdateBuilder_1.default(this.mongoDao)
            .where(filter.where)
            .set(data)
            .update(cb);
    }
    destroy(where, cb = null) {
        return new MongoDeleteBuilder_1.default(this.mongoDao)
            .where(where)
            .delete(cb);
    }
    count(where, cb = null) {
        return new MongoCountBuilder_1.default(this.mongoDao)
            .where(where)
            .count(cb);
    }
}
exports.default = MongoRepository;
//# sourceMappingURL=MongoRepository.js.map