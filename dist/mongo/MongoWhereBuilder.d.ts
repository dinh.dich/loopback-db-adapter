/**
 * Created by DICH on 5/6/2018.
 */
export default class MongoWhereBuilder {
    protected whereClause: any;
    where(whereClause: any): this;
    lt(key: any, value: any): MongoWhereBuilder;
    lte(key: any, value: any): MongoWhereBuilder;
    gt(key: any, value: any): MongoWhereBuilder;
    gte(key: any, value: any): MongoWhereBuilder;
    eq(key: any, value: any): MongoWhereBuilder;
    neq(key: any, value: any): MongoWhereBuilder;
    between(key: any, values: any): MongoWhereBuilder;
    in(key: any, values: any): MongoWhereBuilder;
    nin(key: any, values: any): MongoWhereBuilder;
    exists(key: any, value: any): MongoWhereBuilder;
}
