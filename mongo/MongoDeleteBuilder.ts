/**
 * Created by DICH on 5/6/2018.
 */
import {IMongoDao} from "../IMongoDao";
import MongoWhereBuilder from "./MongoWhereBuilder";

export default class MongoDeleteBuilder extends MongoWhereBuilder {
  private mongoRepos : IMongoDao

  constructor(repos : IMongoDao) {
    super()
    this.mongoRepos = repos
  }

  public delete(cb = null): Promise<any> {
    return new Promise((resolve, reject) => {
      this.mongoRepos.destroyAll(this.whereClause, error => {
        cb && cb(error)
        if (error) {
          reject(error)
        } else {
          resolve()
        }
      })
    })
  }
}
