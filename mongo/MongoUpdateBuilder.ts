/**
 * Created by DICH on 5/6/2018.
 */
import {IMongoDao} from "../IMongoDao";
import MongoWhereBuilder from "./MongoWhereBuilder";

export default class MongoUpdateBuilder extends MongoWhereBuilder {
  private repos : IMongoDao
  private updateInfo : any = {}
  private isUpsert : boolean = false

  constructor(repos : IMongoDao) {
    super()
    this.repos = repos
  }

  public data(info): MongoUpdateBuilder {
    this.updateInfo = info
    return this
  }

  public set(info): MongoUpdateBuilder {
    this.updateInfo['$set'] = info
    return this
  }

  public unset(info): MongoUpdateBuilder {
    this.updateInfo['$unset'] = info
    return this
  }

  public pull(info): MongoUpdateBuilder {
    this.updateInfo['$pull'] = info
    return this
  }

  public push(info): MongoUpdateBuilder {
    this.updateInfo['$push'] = info
    return this
  }

  public pushAll(info): MongoUpdateBuilder {
    this.updateInfo['$pushAll'] = info
    return this
  }

  public upsert(isUpsert): MongoUpdateBuilder {
    this.isUpsert = isUpsert
    return this
  }

  public update(cb = null): Promise<any> {
    return new Promise((resolve, reject) => {
        this.repos.update(this.whereClause, this.updateInfo, (error, {count}) => {
          if (error) {
            cb && cb(error)
            reject(error)
          } else {
            if (!count && this.isUpsert) {
              this.repos.create(this.updateInfo, error => {
                cb && cb(error)
                if (error) {
                  reject(error)
                } else {
                  resolve({count})
                }
              })
            } else {
              cb && cb(null, {count})
              resolve({count})
            }
          }
        })
    })
  }
}
