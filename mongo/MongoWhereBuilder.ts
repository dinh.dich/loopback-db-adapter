/**
 * Created by DICH on 5/6/2018.
 */
export default class MongoWhereBuilder {
  protected whereClause : any = {}

  public where(whereClause) {
    this.whereClause = whereClause
    return this
  }

  public lt(key, value): MongoWhereBuilder {
    this.whereClause = {
      ...this.whereClause,
      [key]: {'lt': value}
    }
    return this
  }

  public lte(key, value): MongoWhereBuilder {
    this.whereClause = {
      ...this.whereClause,
      [key]: {'lte': value}
    }
    return this
  }

  public gt(key, value): MongoWhereBuilder {
    this.whereClause = {
      ...this.whereClause,
      [key]: {'gt': value}
    }
    return this
  }

  public gte(key, value): MongoWhereBuilder {
    this.whereClause = {
      ...this.whereClause,
      [key]: {'gte': value}
    }
    return this
  }

  public eq(key, value): MongoWhereBuilder {
    this.whereClause = {
      ...this.whereClause,
      [key]: {'eq': value}
    }
    return this
  }

  public neq(key, value): MongoWhereBuilder {
    this.whereClause = {
      ...this.whereClause,
      [key]: {'neq': value}
    }
    return this
  }

  public between(key, values): MongoWhereBuilder {
    this.whereClause = {
      ...this.whereClause,
      [key]: {'between': values}
    }
    return this
  }

  public in(key, values): MongoWhereBuilder {
    this.whereClause = {
      ...this.whereClause,
      [key]: {'in': values}
    }
    return this
  }

  public nin(key, values): MongoWhereBuilder {
    this.whereClause = {
      ...this.whereClause,
      [key]: {'nin': values}
    }
    return this
  }

  public exists(key, value): MongoWhereBuilder {
    this.whereClause = {
      ...this.whereClause,
      [key]: {exists: value}
    }
    return this
  }
}
