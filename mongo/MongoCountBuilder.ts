/**
 * Created by DICH on 5/6/2018.
 */
import MongoWhereBuilder from "./MongoWhereBuilder";
import {IMongoDao} from "../IMongoDao";

export default class MongoCountBuilder extends MongoWhereBuilder {
  private mongoRepos : IMongoDao

  constructor(repos : IMongoDao) {
    super()
    this.mongoRepos = repos
  }

  public count(cb = null): Promise<number> {
    return new Promise((resolve, reject) => {
        this.mongoRepos.count(this.whereClause, (error, count) => {
          cb && cb(error, count)
          if (error) {
            reject(error)
          } else {
            resolve(count)
          }
        })
    })
  }
}
