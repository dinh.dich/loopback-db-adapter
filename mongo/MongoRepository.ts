/**
 * Created by DICH on 5/6/2018.
 */
import {IRepository} from "../IRepository";
import {IMongoDao} from "../IMongoDao";
import MongoCreateBuilder from "./MongoCreateBuilder";
import MongoSelectBuilder from "./MongoSelectBuilder";
import MongoUpdateBuilder from "./MongoUpdateBuilder";
import MongoDeleteBuilder from "./MongoDeleteBuilder";
import MongoCountBuilder from "./MongoCountBuilder";

export default class MongoRepository implements IRepository {
  private mongoDao : IMongoDao
  constructor(mongoDao : IMongoDao) {
    this.mongoDao = mongoDao
  }

  getCreateBuilder() : MongoCreateBuilder {
    return new MongoCreateBuilder(this.mongoDao)
  }

  getSelectBuilder() : MongoSelectBuilder {
    return new MongoSelectBuilder(this.mongoDao)
  }

  getUpdateBuilder() : MongoUpdateBuilder {
    return new MongoUpdateBuilder(this.mongoDao)
  }

  getDeleteBuilder() : MongoDeleteBuilder {
    return new MongoDeleteBuilder(this.mongoDao)
  }

  getCountBuilder() {
    return new MongoCountBuilder(this.mongoDao)
  }

  create(info, cb = null) {
    return new MongoCreateBuilder(this.mongoDao)
      .data(info)
      .create(cb)
  }

  all(filter, cb = null): Promise<any> {
    return new MongoSelectBuilder(this.mongoDao)
      .where(filter.where)
      .offset(filter.offset)
      .limit(filter.limit)
      .order(filter.order)
      .fields(filter.fields)
      .select(cb)
  }

  findOne(filter, cb = null): Promise<any> {
    return new MongoSelectBuilder(this.mongoDao)
      .where(filter.where)
      .limit(1)
      .order(filter.order)
      .fields(filter.fields)
      .select()
        .then(results => {
          cb && cb(null, results[0])
          return Promise.resolve(results[0])
        })
        .catch(error => {
          cb && cb(error)
          return Promise.reject(error)
        })
  }

  update(filter, data, cb = null): Promise<any> {
    return new MongoUpdateBuilder(this.mongoDao)
      .where(filter.where)
      .set(data)
      .update(cb)
  }

  destroy(where, cb = null): Promise<any> {
    return new MongoDeleteBuilder(this.mongoDao)
      .where(where)
      .delete(cb)
  }

  count(where, cb = null): Promise<any> {
    return new MongoCountBuilder(this.mongoDao)
      .where(where)
      .count(cb)
  }
}
