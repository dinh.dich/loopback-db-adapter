/**
 * Created by DICH on 5/6/2018.
 */
import {IMongoDao} from "../IMongoDao";
import MongoWhereBuilder from "./MongoWhereBuilder";

export default class MongoSelectBuilder extends MongoWhereBuilder {
  private mongoRepos : IMongoDao
  private filter : any = {}

  constructor(repos : IMongoDao) {
    super()
    this.mongoRepos = repos
  }

  public order(order): MongoSelectBuilder {
    this.filter.order = order
    return this
  }

  public limit(limit): MongoSelectBuilder {
    this.filter.limit = limit
    return this
  }

  public offset(offset): MongoSelectBuilder {
    this.filter.offset = offset
    return this
  }

  public fields(fields): MongoSelectBuilder {
    this.filter.fields = fields
    return this
  }

  public select(cb = null): Promise<any> {
    return new Promise((resolve, reject) => {
        this.mongoRepos.all({...this.filter, where: this.whereClause}, (error, results) => {
          cb && cb(error, results)
          if (error) {
            reject(error)
          } else {
            resolve(results)
          }
        })
    })
  }

  public findOne(cb = null): Promise<any> {
    this.limit(1)
    return this.select()
        .then(results => {
          cb && cb(null, results[0])
          return Promise.resolve(results[0])
        })
        .catch(error => {
          cb && cb(error)
          return Promise.reject(error)
        })
  }
}
