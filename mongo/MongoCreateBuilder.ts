/**
 * Created by DICH on 5/6/2018.
 */
import {IMongoDao} from "../IMongoDao";

export default class MongoCreateBuilder {
  private mongoRepos : IMongoDao
  private info : object = {}

  constructor(repos : IMongoDao) {
    this.mongoRepos = repos
  }

  public data(info: object): MongoCreateBuilder {
    this.info = info
    return this
  }

  public create(cb = null): Promise<object> {
    return new Promise((resolve, reject) => {
        this.mongoRepos.create(this.info, (error, info: object) => {
          cb && cb(error, info)
          if (error) {
            reject(error)
          } else {
            resolve(info)
          }
        })
    })
  }
}
