/**
 * Created by DichVD on 5/7/2018.
 */

export interface IElasticDao {
    create(data, callback);
    all(filter, callback);
    updateAll(where, data, options, callback);
    updateOrCreate(data, callback)
    updateAllScript(where, data, options, callback)
    updateScript(id, data, callback)
    updateOrCreateScript(id, data, callback)
    destroyAll(where, callback);
    find(id, callback);
    destroy(id, callback);
    exists(id, callback);
    save(data, callback);
    search(query, callback);
    update(query, callback);
    count(where, callback)
}
