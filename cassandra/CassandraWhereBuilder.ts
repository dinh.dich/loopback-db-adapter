/**
 * Created by DICH on 5/6/2018.
 */
export default class CassandraWhereBuilder {
  protected whereClause : any = {}

  protected getWhereClause(): {} {
    if (this.whereClause && Object.keys(this.whereClause).length > 0) return this.whereClause
  }

  public where(whereClause: object) {
    this.whereClause = whereClause
    return this
  }

  public lt(key, value) {
    this.whereClause = {
      ...this.whereClause,
      [key]: {'lt': value}
    }
    return this
  }

  public lte(key, value) {
    this.whereClause = {
      ...this.whereClause,
      [key]: {'lte': value}
    }
    return this
  }

  public gt(key, value) {
    this.whereClause = {
      ...this.whereClause,
      [key]: {'gt': value}
    }
    return this
  }

  public gte(key, value) {
    this.whereClause = {
      ...this.whereClause,
      [key]: {'gte': value}
    }
    return this
  }

  public eq(key, value) {
    this.whereClause = {
      ...this.whereClause,
      [key]: {'eq': value}
    }
    return this
  }
}

