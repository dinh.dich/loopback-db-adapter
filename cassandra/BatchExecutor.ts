/**
 * Created by DichVD on 5/7/2018.
 */
abstract class BatchExecutor {
    public abstract create(data, options) : BatchExecutor
    public abstract update(where, data, checkExists) : BatchExecutor
    public abstract delete(where, columns) : BatchExecutor
    public abstract execute(cb): Promise<any>
}

export default BatchExecutor