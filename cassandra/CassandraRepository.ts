/**
 * Created by DICH on 5/6/2018.
 */
import {IRepository} from "../IRepository";
import {ICassandraDao} from "../ICassandraDao";
import CassandraCreateBuilder from "./CassandraCreateBuilder";
import CassandraSelectBuilder from "./CassandraSelectBuilder";
import CassandraUpdateBuilder from "./CassandraUpdateBuilder";
import CassandraDeleteBuilder from "./CassandraDeleteBuilder";
import BatchExecutor from "./BatchExecutor";
import {Observable} from 'rx'

export default class CassandraRepository implements IRepository {
  private cassandraDao : ICassandraDao
  constructor(mongoDao : ICassandraDao) {
    this.cassandraDao = mongoDao
  }

  getCreateBuilder() : CassandraCreateBuilder {
    return new CassandraCreateBuilder(this.cassandraDao)
  }

  getSelectBuilder() : CassandraSelectBuilder {
    return new CassandraSelectBuilder(this.cassandraDao)
  }

  getUpdateBuilder() : CassandraUpdateBuilder {
    return new CassandraUpdateBuilder(this.cassandraDao)
  }

  getDeleteBuilder() : CassandraDeleteBuilder {
    return new CassandraDeleteBuilder(this.cassandraDao)
  }

  getCountBuilder(): any {
    throw new Error("Method not implemented.");
  }

  getBatchBuilder() : BatchExecutor {
    return this.cassandraDao.batch()
  }

  count(where: any, cb: any = null): any {
    throw new Error("Method not implemented.");
  }

  create(info, cb = null): Promise<any> {
    return new CassandraCreateBuilder(this.cassandraDao)
      .data(info)
      .create(cb)
  }

  all(filter, cb = null): Promise<Array<any>> {
    return new CassandraSelectBuilder(this.cassandraDao)
      .where(filter.where)
      .offset(filter.offset)
      .limit(filter.limit)
      .order(filter.order)
      .fields(filter.fields)
      .select(cb)
  }

  findOne(filter, cb = null): Promise<object> {
    return new CassandraSelectBuilder(this.cassandraDao)
      .where(filter.where)
      .limit(1)
      .order(filter.order)
      .fields(filter.fields)
      .select()
        .then(results => {
          cb && cb(null, results[0])
          return Promise.resolve(results[0])
        })
        .catch(error => {
          cb && cb(error)
          return Promise.reject(error)
        })
  }

  update(filter, data, cb = null): Promise<any> {
    return new CassandraUpdateBuilder(this.cassandraDao)
      .where(filter.where)
      .data(data)
      .update(cb)
  }

  destroy(where, cb = null): Promise<any> {
    return new CassandraDeleteBuilder(this.cassandraDao)
      .where(where)
      .delete(cb)
  }

  eachPage(filter, options, eachCb, endCb): Observable {
    return new CassandraSelectBuilder(this.cassandraDao)
        .where(filter.where)
        .offset(filter.offset)
        .limit(filter.limit)
        .order(filter.order)
        .fields(filter.fields)
        .options(options)
        .eachPage(eachCb, endCb)
  }
}
