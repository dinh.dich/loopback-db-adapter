/**
 * Created by DICH on 5/6/2018.
 */
import {ICassandraDao} from "../ICassandraDao";

export default class CassandraCreateBuilder {
  private cassandraRepos : ICassandraDao
  private info : any = {}

  constructor(repos : ICassandraDao) {
    this.cassandraRepos = repos
  }

  public data(info): CassandraCreateBuilder {
    this.info = info
    return this
  }

  public create(cb = null): Promise<any> {
    return new Promise((resolve, reject) => {
        this.cassandraRepos.create(this.info, error => {
          cb && cb(error)
          if (error) {
            reject(error)
          } else {
            resolve()
          }
        })
    })
  }
}
