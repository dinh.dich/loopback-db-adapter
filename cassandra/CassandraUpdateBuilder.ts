/**
 * Created by DICH on 5/6/2018.
 */
import CassandraWhereBuilder from "./CassandraWhereBuilder";
import {ICassandraDao} from "../ICassandraDao";

export default class CassandraUpdateBuilder extends CassandraWhereBuilder {
  private cassandraRepos : ICassandraDao
  private updateInfo : any = {}
  private isUpsert : boolean = true

  constructor(repos : ICassandraDao) {
    super()
    this.cassandraRepos = repos
  }

  public data(info): CassandraUpdateBuilder {
    this.updateInfo = info
    return this
  }

  public upsert(isUpsert): CassandraUpdateBuilder {
    this.isUpsert = isUpsert
    return this
  }

  public update(cb = null): Promise<any> {
    return new Promise((resolve, reject) => {
        this.cassandraRepos.update(this.getWhereClause(), this.updateInfo, !this.isUpsert, error => {
          cb && cb(error)
          if (error) {
            reject(error)
          } else {
            resolve()
          }
        })
    })
  }
}
