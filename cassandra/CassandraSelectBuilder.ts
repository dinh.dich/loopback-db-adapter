/**
 * Created by DICH on 5/6/2018.
 */
import CassandraWhereBuilder from "./CassandraWhereBuilder";
import {ICassandraDao} from "../ICassandraDao";
import {Observable} from 'rx'
import {EachCb, EndCb} from './CassandraTypes'

export default class CassandraSelectBuilder extends CassandraWhereBuilder {
  private cassandraRepos : ICassandraDao
  private filter : any = {}
  private eachPageOptions : any

  constructor(repos : ICassandraDao) {
    super()
    this.cassandraRepos = repos
  }

  public order(order): CassandraSelectBuilder {
    this.filter.order = order
    return this
  }

  public limit(limit): CassandraSelectBuilder {
    this.filter.limit = limit
    return this
  }

  public offset(offset): CassandraSelectBuilder {
    this.filter.offset = offset
    return this
  }

  public fields(fields): CassandraSelectBuilder {
    this.filter.fields = fields
    return this
  }

  public options(options : {autoPage: boolean, fetchSize: number}): CassandraSelectBuilder {
    this.eachPageOptions = options
    return this
  }

  public select(cb = null): Promise<Array<any>> {
    return new Promise((resolve, reject) => {
        this.cassandraRepos.all({...this.filter, where: this.getWhereClause()}, (error, results) => {
          cb && cb(error, results)
          if (error) {
            reject(error)
          } else {
            resolve(results)
          }
        })
    })
  }

  public findOne(cb = null): Promise<any> {
    this.limit(1)
    return this.select()
        .then(results => {
          cb && cb(null, results[0])
          return Promise.resolve(results[0])
        })
        .catch(error => {
          cb && cb(error)
          return Promise.reject(error)
        })
  }

  public eachPage(eachCb: EachCb, endCb: EndCb): Observable {
    return Observable.create(observer => {
      this.cassandraRepos.eachPage({...this.filter, where: this.getWhereClause()}, this.eachPageOptions, (page, rows) => {
        eachCb && eachCb(page, rows)
        observer.onNext(rows)
      }, (error, result) => {
        endCb && endCb(error, result)
        if (error) {
          observer.onError(error)
        } else {
          observer.onCompleted()
        }
      })
    })
  }


}
