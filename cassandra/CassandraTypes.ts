/**
 * Created by dichn on 9/16/2018.
 */
export type EachCb = (page: number, rows: Array<Object>) => any
export type EndCb = (error: object, result: {nextPage: Function}) => any
