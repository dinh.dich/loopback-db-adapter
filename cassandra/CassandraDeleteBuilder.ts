/**
 * Created by DICH on 5/6/2018.
 */
import CassandraWhereBuilder from "./CassandraWhereBuilder";
import {ICassandraDao} from "../ICassandraDao";

export default class CassandraDeleteBuilder extends CassandraWhereBuilder {
  private cassandraRepos : ICassandraDao
  private columns : Array<string>

  constructor(repos : ICassandraDao) {
    super()
    this.cassandraRepos = repos
  }

  public fields(fields): CassandraDeleteBuilder {
    this.columns = fields
    return this
  }

  public delete(cb = null): Promise<any> {
    return new Promise((resolve, reject) => {
      this.cassandraRepos.destroy(this.getWhereClause(), this.columns, error => {
        cb && cb(error)
        if (error) {
          reject(error)
        } else {
          resolve()
        }
      })
    })
  }
}
