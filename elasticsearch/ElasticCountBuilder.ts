/**
 * Created by DICH on 5/6/2018.
 */
import ElasticWhereBuilder from "./ElasticWhereBuilder";
import {IElasticDao} from "../IElasticDao";

export default class ElasticCountBuilder extends ElasticWhereBuilder {
  private elasticRepos : IElasticDao

  constructor(repos : IElasticDao) {
    super()
    this.elasticRepos = repos
  }

  public count(cb = null): Promise<any> {
    return new Promise((resolve, reject) => {
        this.elasticRepos.count(this.whereClause, (error, count) => {
          cb && cb(error, count)
          if (error) {
            reject(error)
          } else {
            resolve(count)
          }
        })
    })
  }
}
