/**
 * Created by DICH on 5/6/2018.
 */
export default class ElasticWhereBuilder {
  protected whereClause : any = {}
  protected matchClause : any = {}
  private mode = 0

  public where(whereClause) {
    this.whereClause = whereClause || this.whereClause
    this.mode = 0
    return this
  }

  public match(matchClause) {
    this.matchClause = matchClause || this.matchClause
    this.mode = 1
    return this
  }

  public lt(key, value) {
    if (this.mode == 0) {
      this.whereClause = {
        ...this.whereClause,
        [key]: {'lt': value}
      }
    } else {
      this.matchClause = {
        ...this.matchClause,
        [key]: {'lt': value}
      }
    }

    return this
  }

  public lte(key, value) {
    if (this.mode == 0) {
      this.whereClause = {
        ...this.whereClause,
        [key]: {'lte': value}
      }
    } else {
      this.matchClause = {
        ...this.matchClause,
        [key]: {'lte': value}
      }
    }

    return this
  }

  public gt(key, value) {
    if (this.mode == 0) {
      this.whereClause = {
        ...this.whereClause,
        [key]: {'gt': value}
      }
    } else {
      this.matchClause = {
        ...this.matchClause,
        [key]: {'gt': value}
      }
    }

    return this
  }

  public gte(key, value) {
    if (this.mode == 0) {
      this.whereClause = {
        ...this.whereClause,
        [key]: {'gte': value}
      }
    } else {
      this.matchClause = {
        ...this.matchClause,
        [key]: {'gte': value}
      }
    }

    return this
  }

  public eq(key, value) {
    if (this.mode == 0) {
      this.whereClause = {
        ...this.whereClause,
        [key]: value
      }
    } else {
      this.matchClause = {
        ...this.matchClause,
        [key]: value
      }
    }

    return this
  }

  public neq(key, value) {
    if (this.mode == 0) {
      this.whereClause = {
        ...this.whereClause,
        [key]: {'neq': value}
      }
    } else {
      this.matchClause = {
        ...this.matchClause,
        [key]: {'neq': value}
      }
    }

    return this
  }

  public between(key, values) {
    if (this.mode == 0) {
      this.whereClause = {
        ...this.whereClause,
        [key]: {'between': values}
      }
    } else {
      this.matchClause = {
        ...this.matchClause,
        [key]: {'between': values}
      }
    }

    return this
  }

  public in(key, values) {
    if (this.mode == 0) {
      this.whereClause = {
        ...this.whereClause,
        [key]: {'inq': values}
      }
    } else {
      this.matchClause = {
        ...this.matchClause,
        [key]: {'inq': values}
      }
    }

    return this
  }

  public nin(key, values) {
    if (this.mode == 0) {
      this.whereClause = {
        ...this.whereClause,
        [key]: {'nin': values}
      }
    } else {
      this.matchClause = {
        ...this.matchClause,
        [key]: {'nin': values}
      }
    }

    return this
  }

  public exists(key, value) {
    if (this.mode == 0) {
      this.whereClause = {
        ...this.whereClause,
        [key]: {exists: value}
      }
    } else {
      this.matchClause = {
        ...this.matchClause,
        [key]: {exists: value}
      }
    }

    return this
  }

  public or(key, value) {
    if (this.mode == 0) {
      this.whereClause = {
        ...this.whereClause,
        or: [...(this.whereClause.or || []), {[key]: value}]
      }
    } else {
      this.matchClause = {
        ...this.matchClause,
        or: [...(this.matchClause.or || []), {[key]: value}]
      }
    }

    return this
  }

  public and(key, value) {
    if (this.mode == 0) {
      this.whereClause = {
        ...this.whereClause,
        and: [...(this.whereClause.and || []), {[key]: value}]
      }
    } else {
      this.matchClause = {
        ...this.matchClause,
        and: [...(this.matchClause.and || []), {[key]: value}]
      }
    }

    return this
  }

  public script(script) {
    if (this.mode == 0) {
      this.whereClause = {
        ...this.whereClause,
        '$script': script
      }
    } else {
      this.matchClause = {
        ...this.matchClause,
        '$script': script
      }
    }
    return this
  }
}
