/**
 * Created by DICH on 5/6/2018.
 */
import ElasticWhereBuilder from "./ElasticWhereBuilder";
import {IElasticDao} from "../IElasticDao";

export default class ElasticDeleteBuilder extends ElasticWhereBuilder {
  private elasticRepos : IElasticDao

  constructor(repos : IElasticDao) {
    super()
    this.elasticRepos = repos
  }

  public delete(cb = null): Promise<any> {
    return new Promise((resolve, reject) => {
      this.elasticRepos.destroyAll(this.whereClause, error => {
        cb && cb(error)
        if (error) {
          reject(error)
        } else {
          resolve()
        }
      })
    })
  }
}
