/**
 * Created by DICH on 5/6/2018.
 */
import {IElasticDao} from "../IElasticDao";

export default class ElasticCreateBuilder {
  private elasticRepos : IElasticDao
  private info : any = {}

  constructor(repos : IElasticDao) {
    this.elasticRepos = repos
  }

  public data(info): ElasticCreateBuilder {
    this.info = info
    return this
  }

  public create(cb = null): Promise<any> {
    return new Promise((resolve, reject) => {
        this.elasticRepos.create(this.info, error => {
          cb && cb(error)
          if (error) {
            reject(error)
          } else {
            resolve()
          }
        })
    })
  }
}
