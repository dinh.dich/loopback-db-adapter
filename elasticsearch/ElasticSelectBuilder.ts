/**
 * Created by DICH on 5/6/2018.
 */
import ElasticWhereBuilder from "./ElasticWhereBuilder";
import {IElasticDao} from "../IElasticDao";

export default class ElasticSelectBuilder extends ElasticWhereBuilder {
  private elasticRepos : IElasticDao
  private filter : any = {}

  constructor(repos : IElasticDao) {
    super()
    this.elasticRepos = repos
  }

  public order(order): ElasticSelectBuilder {
    this.filter.order = order
    return this
  }

  public limit(limit): ElasticSelectBuilder {
    this.filter.limit = limit
    return this
  }

  public offset(offset): ElasticSelectBuilder {
    this.filter.offset = offset
    return this
  }

  public fields(fields): ElasticSelectBuilder {
    this.filter.fields = fields
    return this
  }

  /**
   * add function score
   * @param score
   * @returns {ElasticSelectBuilder}
   */
  public score(score): ElasticSelectBuilder {
    this.filter.score = score
    return this
  }

  public select(cb = null): Promise<any> {
    return new Promise((resolve, reject) => {
        this.elasticRepos.all({...this.filter, where: this.whereClause, match: this.matchClause}, (error, results) => {
          cb && cb(error, results)
          if (error) {
            reject(error)
          } else {
            resolve(results)
          }
        })
    })
  }

  public findOne(cb = null): Promise<any> {
    this.limit(1)
    return this.select()
        .then(results => {
          cb && cb(null, results[0])
          return Promise.resolve(results[0])
        })
        .catch(error => {
          cb && cb(error)
          return Promise.reject(error)
        })
  }
}
