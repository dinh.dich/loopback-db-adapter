/**
 * Created by DICH on 5/6/2018.
 */
import {IRepository} from "../IRepository";
import {IElasticDao} from "../IElasticDao";
import ElasticCreateBuilder from "./ElasticCreateBuilder";
import ElasticSelectBuilder from "./ElasticSelectBuilder";
import ElasticUpdateBuilder from "./ElasticUpdateBuilder";
import ElasticDeleteBuilder from "./ElasticDeleteBuilder";
import ElasticCountBuilder from "./ElasticCountBuilder";

export default class ElasticRepository implements IRepository {
  private elasticDao : IElasticDao
  constructor(mongoDao : IElasticDao) {
    this.elasticDao = mongoDao
  }

  getCreateBuilder() : ElasticCreateBuilder {
    return new ElasticCreateBuilder(this.elasticDao)
  }

  getSelectBuilder() : ElasticSelectBuilder {
    return new ElasticSelectBuilder(this.elasticDao)
  }

  getUpdateBuilder() : ElasticUpdateBuilder {
    return new ElasticUpdateBuilder(this.elasticDao)
  }

  getDeleteBuilder() : ElasticDeleteBuilder {
    return new ElasticDeleteBuilder(this.elasticDao)
  }

  getCountBuilder() : ElasticCountBuilder {
    return new ElasticCountBuilder(this.elasticDao)
  }

  create(info, cb = null): Promise<any> {
    return new ElasticCreateBuilder(this.elasticDao)
      .data(info)
      .create(cb)
  }

  all(filter, cb = null): Promise<any> {
    return new ElasticSelectBuilder(this.elasticDao)
      .where(filter.where)
      .offset(filter.offset)
      .limit(filter.limit)
      .order(filter.order)
      .fields(filter.fields)
      .select(cb)
  }

  findOne(filter, cb = null): Promise<any> {
    return new ElasticSelectBuilder(this.elasticDao)
      .where(filter.where)
      .limit(1)
      .order(filter.order)
      .fields(filter.fields)
      .select()
        .then(results => {
          cb && cb(null, results[0])
          return Promise.resolve(results[0])
        })
        .catch(error => {
          cb && cb(error)
          return Promise.reject(error)
        })
  }

  update(filter, data, cb = null): Promise<any> {
    return new ElasticUpdateBuilder(this.elasticDao)
      .where(filter.where)
      .data(data)
      .update(cb)
  }

  destroy(where, cb = null): Promise<any> {
    return new ElasticDeleteBuilder(this.elasticDao)
      .where(where)
      .delete(cb)
  }

  count(where, cb = null): Promise<any> {
    return new ElasticCountBuilder(this.elasticDao)
      .where(where)
      .count(cb)
  }
}
