/**
 * Created by DICH on 5/6/2018.
 */

export interface IMongoDao {
  create(data, callback);
  all(filter, callback);
  update(where, data, callback);
  destroyAll(where, callback);
  findOne(where, callback);
  count(where, callback)
}
