/**
 * Created by DICH on 5/6/2018.
 */
interface IRepository {
  getCreateBuilder()
  getSelectBuilder()
  getUpdateBuilder()
  getDeleteBuilder()
  getCountBuilder()

  create(info, cb)
  all(filter, cb)
  findOne(filter, cb)
  update(filter, data, cb)
  destroy(where, cb)
  count(where, cb)
}

export {IRepository}
