/**
 * Created by DICH on 5/6/2018.
 */

interface ICassandraDao {
  create(data, callback) : any;
  all(filter, callback)
  update(where, data, checkExists, callback) : any;
  destroy(where, columns, callback) : any;
  eachPage(filter, options, eachCb, endCb) : any;
  batch() : any;
}

export {ICassandraDao}
